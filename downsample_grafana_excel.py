from cProfile import label
from cgi import test
from dataclasses import asdict
from datetime import datetime, timedelta
from multiprocessing.sharedctypes import Value
from tracemalloc import start
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

grafana_file = r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-269 NSF fridge testing - dough storage\grafana.xlsx'

# changed the column names in the spreadsheets
topping_channels = ['ch4','ch5','ch6','ch7','air_temperature_c','defrost_temperature_c','setpoint temp']
dough_storage_channels = ['center_temp_c', 'pick temp', 'xp floor temp', 'air_temperature_c', 'defrost_temperature_c', 'setpoint temp']


# grafana has
# data rate = ~4 Hz
# compressor is 0 or 1 not boolean
# has many NaNs
# ----------------


class FridgeData():
    '''
    filepath (str): full path
    module (str): 'topping-line' or 'dough-storage'
    plot (bool): whether to plot temps and compressor duty cycle
    save (bool): whether to save the data. File will be at the same location but
        with an addended name
    
    should save time

    '''


    def __init__(self, file_path, module, plot=False, save=False):

        topping_channels = ['Time', 'ch4','ch5','ch6','ch7','air_temperature_c','defrost_temperature_c','setpoint temp']
        dough_storage_channels = ['Time', 'center_temp_c', 'pick temp', 'xp floor temp', 'air_temperature_c', 'defrost_temperature_c', 'setpoint temp']

        self.module = module
        if module == 'dough-storage':
            self.channels = dough_storage_channels + ['compressor']
            self.temp_channels = dough_storage_channels
        elif module == 'topping-line':
            self.channels = topping_channels + ['compressor']
            self.temp_channels = topping_channels
        else:
            raise ValueError('invalid entry for module')

        self.path = file_path
        self.save_path = file_path.strip('.csv') + '_trimmed.xlsx'

        self.load_and_clean_data()
        self.save_data()

    def load_and_clean_data(self):
        # self.df = pd.read_excel(self.path)
        # print(self.channels)
        self.df = pd.read_csv(self.path, usecols=self.channels)
        # print(self.df.head(n=5))
        # print(self.df.columns)

        # set time to index
        self.df.set_index('Time', drop=True, inplace=True)
        # convert bools to ints
        self.df['compressor'] = self.df["compressor"].replace({True: 1, False: 0})
        # print(self.df.head(n=15))

        # grafana has duplicate index columns, both because time is read in seconds but
        # the sampling rate is 4 Hz, and also because the compressor data gets its own line.
        # group the data and downselect to 1Hz.
        self.df = self.df.groupby(level=0).first()
        self.df.dropna(inplace=True)
        # print(self.df.head(n=15))

        # may need to convert grafana_df to datetime values.
        self.df['Time'] = pd.to_datetime(self.df.index)
        self.df.set_index('Time', drop=True, inplace=True)

        # downsample grafana data to go to 1 Hz. backfill NaNs.
        self.df = self.df.resample('1S').bfill()
        # print(self.df.head(n=15))

    def save_data(self):
        # Save if desired
        self.df.to_excel(self.save_path)

    def analyze_and_plot(self, start_time, end_time):
        '''
        start and end time format should be: yyyy-mm-dd hh:mm:ss
        '''

        self.df.plot()
        plt.show()

        # select time ranges
        test_data_df = self.df.truncate(start_time, end_time)
        test_data_df.plot()
        plt.show()

        # there's some cleaner plotting to do here with subplots of similar data streams
        f, (ax1, ax2) = plt.subplots(2, 1, sharex='all')

        # temps
        for col in test_data_df.columns:
            if col in self.temp_channels:
                ax1.plot(test_data_df.index.values, test_data_df[col].values, label=col)
        # ax1.set_ylim()
        # ax1.axhline(y=36, color='r')
        ax1.set_ylabel('Temperature [c]')
        ax1.legend()

        # compressor cycle
        ax2.plot(test_data_df.index.values, test_data_df['compressor'], label='compressor')
        ax2.set_ylabel('compressor on/off')
        ax2.legend()
        ax2.set_xlabel('Time')

        duty_cycle = test_data_df['compressor'].sum() / test_data_df.shape[0] * 100
        f.suptitle(f'{self.module} results\n duty cycle {duty_cycle:.2f}%\n \
            start time: {start_time} | end time: {end_time}',
            fontsize=10)
        plt.show()

        # calculate duty cycle of the entire time
        print(f'{duty_cycle:.2f}')

test_cases = {
    'test-359 round 1': {
        'dough-storage': {
            'path': r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-265 Fridge testing\Test-359 fridge comparison\round 1\dough-storage temperature-data-as-seriestocolumns-2022-07-20 20_28_08.csv',
            'phase 1': {'start time': '2022-07-08 14:28:00', 'end time': '2022-07-08 15:22:00'},
            'phase 2': {'start time': '2022-07-08 15:39:00', 'end time': '2022-07-08 16:32:00'}
        },
        'topping-line': {
            'path': r"C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-265 Fridge testing\Test-359 fridge comparison\round 1\topping-line temperatures-data-as-seriestocolumns-2022-07-20 23_13_09.csv",
            'phase 1': {'start time': '2022-07-08 13:30:00', 'end time': '2022-07-08 15:19:00'},
            'phase 2': {'start time': '2022-07-08 15:32:00', 'end time': '2022-07-08 16:25:00'}
        }   
    },
    'test-359 round 2': {
        'dough-storage': {
            'path': r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-265 Fridge testing\Test-359 fridge comparison\round 2\dough-storage temperature-data-as-seriestocolumns-2022-07-21 12_07_14.csv',
            'phase 1': {'start time': '2022-07-14 13:13:00', 'end time': '2022-07-14 14:26:00'},
            'phase 2': {'start time': '2022-07-14 11:43:00', 'end time': '2022-07-14 12:57:00'}
        },
        'topping-line': {
            'path': r"C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-265 Fridge testing\Test-359 fridge comparison\round 2\topping-line temperatures-data-as-seriestocolumns-2022-07-21 12_07_26.csv",
            'phase 1': {'start time': '2022-07-14 13:06:00', 'end time': '2022-07-14 14:23:00'},
            'phase 2': {'start time': '2022-07-14 10:57:00', 'end time': '2022-07-14 12:50:00'}
        }   
    }
}


def run():
    test_case = 'test-359 round 2'
    module = 'dough-storage'
    file_path = test_cases[test_case][module]['path']
    start_time = test_cases[test_case][module]['phase 1']['start time']
    end_time = test_cases[test_case][module]['phase 1']['end time']

    fridge_data = FridgeData(file_path, module=module, plot=True, save=False)
    fridge_data.analyze_and_plot(start_time, end_time)

run()