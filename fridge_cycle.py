from nturl2path import url2pathname
import requests
import time
import threading

# make sure to set parameters before doing this.
# saw_velocity
# slicer_gantry cmd_velocity_mmps
# slicer_gantry cmd_acceleration_mmpss

def get_state(station):
    # get the current state
    response = requests.get(
        'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{}/parameters'.format(station))
    # print(response.json())
    return response.json()['state']


def entry(station):
    # command entry routine
    requests.post(
        'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{}/actions/entry_routine'.format(station))

    # # sleep until state stops being wait for exit
    # while get_state(station) == 'entry_routine':
    #     time.sleep(1)


def exit(station):
    # command exit routine
    requests.post(
        # 'http://kitchen3.corp.serveautomation.com/api/rpc/stations/pepperoni/actions/manual')
        'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{}/actions/exit_routine'.format(station))

    # # wait until ready again
    # while get_state(station) != 'wait_for_entry':
    # # while get_state() != 'manual':
    #     time.sleep(1)


def run_stations(n, stations):
    for i in range(n):
        print(f'run {i}')
        for station in stations:
            print(f'entry: {station}')
            # while 1:
            print('starting entry')
            # start all three entries
            entry(station)

        # wait for all three to be done with entry
        while(any(get_state(s) == 'entry_routine' for s in stations)):
            print('waiting for entries to be done...')
            time.sleep(4)

        for station in stations:
            print(f'exit: {station}')
            # start all three exits
            exit(station)

        while(any(get_state(s) == 'exit_routine' for s in stations)):
                print('waiting for exits to be done...')
                time.sleep(4)

def run_station(n, station):
    for i in range(n):
        print(f'run {i}')
        print(f'entry: {station}')
        # while 1:
        # print('starting entry')
        # start all three entries
        entry(station)

        # wait for all three
        while(get_state(station) == 'entry_routine'):
            # print('waiting for entry to be done...')
            time.sleep(4)

        print(f'exit: {station}')
        exit(station)

        while (get_state(station) == 'exit_routine'):
            # print(f'{station}: waiting for exit to be done...')
            if station == 'dough-storage':
                url = f'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{station}/parameters'
                params = {'waiting_for_input': False}
                requests.patch(url=url, json=params)
            time.sleep(4)


def run():
    stations = ['dough-storage','sauce-n-cheese','configurable-toppings']

    threads = [
        threading.Thread(target=run_station, args=(500, station,)) for station in stations
    ]

    for t in threads:
        t.start()

    for t in threads:
        t.join()
