import requests
import time

# TODO
# need to verify the ranges for mixer_step

# make sure to set speed parameters before doing this.

def fridge_on():

    # turn on the refrigeration units and wait until They are in startup or wait for entry
    # this is through power-distribution
    url = 'http://kitchen3.corp.serveautomation.com/api/rpc/stations/power-distribution/parameters'
    params={
        'loads.dough_refrigeration.cmd_enabled': True,
        'loads.topping_refrigeration.cmd_enabled': True
    }
    requests.patch(url, json=params)
    response =  get_state('power-distribution')
    # print(response.json()['loads.dough_refrigeration.cmd_enabled'])

def turn_on_cooling():
    url = 'http://kitchen3.corp.serveautomation.com/api/rpc/stations/dough-refrigeration/parameters'
    params={
        'control.cmd_automatic': True,
    }
    requests.patch(url, json=params)
    url = 'http://kitchen3.corp.serveautomation.com/api/rpc/stations/topping-refrigeration/parameters'
    params={
        'control.cmd_automatic': True,
    }
    requests.patch(url, json=params)

def check_on_success(n):
    # check that all states are desired up to n times
    for i in range(5):
        dough_on = get_state('power-distribution').json()['loads.dough_refrigeration.cmd_enabled']
        topping_on = get_state('power-distribution').json()['loads.topping_refrigeration.cmd_enabled']
        # returns true if all are true
        result = all([dough_on, topping_on])
        if result:
            break
        time.sleep(1)
    return result

def check_cool_success(n):
    # check that all states are desired up to n times
    for i in range(5):
        dough_state = get_state('dough-refrigeration').json()['control.cmd_automatic']
        topping_state = get_state('topping-refrigeration').json()['control.cmd_automatic']
        # returns true if all are true
        result = all([dough_state, topping_state])
        if result:
            break
        time.sleep(1)
    return result

def get_state(station):
    url = 'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{}/parameters'.format(station)
    response = requests.get(url)
    return response

def run():
    fridge_on()
    res = check_on_success(5)
    if not res:
        print('fridges could not turn on')
        return
    turn_on_cooling()
    res = check_cool_success(5)
    if not res:
        print('fridges could not be set to cool')

# run()