import requests
import time

# TODO
# need to verify the ranges for mixer_step

# make sure to set speed parameters before doing this.

def cycle(dispenser_step, direction, mixer_step, num_steps, delay=5):
    # steps the motor spinner through combined stirrer and dispenser.

    # command exit routine
    params = {
        'motor_a.cmd_mode': 'position',
        # 'motor_a.cmd_position_deg': 0,
        'motor_a.cmd_velocity_rps': 1,
        'motor_b.cmd_mode': 'position',
        # 'motor_b.cmd_position_deg': 0,
        'motor_b.cmd_velocity_rps': 1,
        }
    url = 'http://kitchen-data.corp.serveautomation.com/api/rpc/stations/motor-spinner/parameters'
    requests.patch(url, json=params)

    # sleep to let params take
    time.sleep(0.5)

    response =  get_state()
    initial_mixer_cmd = response.json()['motor_b.position_deg']
    initial_dispense_cmd = response.json()['motor_a.position_deg']

    for i in range(num_steps):
        mixer_cmd =  mixer_step * i + initial_mixer_cmd
        print(i)
        print(i // 2)
        if direction == 'forward':
            # move the dispenser and mixer
            # assume starting at 0...
            dispense_cmd = dispenser_step * i + initial_dispense_cmd
            
        elif direction == 'switch':
            # oscillate between the command and zero.
            if i % 2 == 0:
                dispense_cmd = dispenser_step + initial_dispense_cmd
            else:
                dispense_cmd = 0 + initial_dispense_cmd

        params = {
        'motor_a.cmd_position_deg': dispense_cmd, 
        'motor_b.cmd_position_deg': mixer_cmd, 
        }
        requests.patch(url, json=params)

        print('step: {}\n dispenser position: {}\n mixer_position: {}'.format(i,dispense_cmd, mixer_cmd))


        # sleep until motion is done + time for operator to record the data.
        response =  get_state()
        while(abs(response.json()['motor_a.position_deg'] - dispense_cmd) > 5 or
           abs(response.json()['motor_b.position_deg'] - mixer_cmd) > 5):
           # update response and re-check. Time delay of 1s to not flood server.
           response =  get_state()
           time.sleep(1)

        time.sleep(delay)

def get_state():
    url = 'http://kitchen-data.corp.serveautomation.com/api/rpc/stations/motor-spinner/parameters'
    response = requests.get(url)
    return response



if __name__ == "__main__":
    # https://www.osmtec.com/23HS22-2804S-PG47
    gear_ratio = 47.5
    # https://pdf1.alldatasheet.com/datasheet-pdf/view/1142317/STEPPERONLINE/23HS22-2804D-PG4-E1000.html
    gear_ratio = 4.25


    cycle(dispenser_step=720,
        direction='switch',
        mixer_step=gear_ratio * 360 / 2,
        num_steps=10,
        delay=5)