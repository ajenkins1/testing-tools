import requests
import time

# make sure to set parameters before doing this.
# saw_velocity
# slicer_gantry cmd_velocity_mmps
# slicer_gantry cmd_acceleration_mmpss

def get_state(station):
    # get the current state
    url = f'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{station}/parameters'
    response = requests.get(url)
    # print(response.json())
    return response.json()['state']


def entry_and_wait(station):
    # command entry routine
    url = f'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{station}/actions/entry_routine'
    requests.post(url)

    # sleep until state stops being wait for exit
    while get_state(station) == 'entry_routine':
        time.sleep(1)


def exit_and_wait(station):
    # command exit routine
    url = f'http://kitchen3.corp.serveautomation.com/api/rpc/stations/{station}/actions/exit_routine'
    requests.post(url)
        # 'http://kitchen3.corp.serveautomation.com/api/rpc/stations/pepperoni/actions/manual')
    # wait until ready again
    while get_state(station) != 'wait_for_entry':
    # while get_state() != 'manual':
        time.sleep(1)


def run(n, station):
    for i in range(n):
        print(f'run {i}')
        # while 1:
        print('starting entry')
        entry_and_wait(station)
        print('starting exit')
        exit_and_wait(station)

run(40, 'dough-press')
