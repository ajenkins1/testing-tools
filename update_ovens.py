import requests
import time

# make sure to set parameters before doing this.

upper_temp_delta = 20
lower_temp_delta = -1 * upper_temp_delta

param_1 = {
    'griddle.control_temp_delta_upper_c': 2,
    'griddle.control_temp_delta_lower_c': -2,
    'griddle.cmd_preheat_temp_c': 275.0,
    'griddle.cmd_temp_c': 275.0,
    'inner_broiler.control_temp_delta_upper_c': 10,
    'inner_broiler.control_temp_delta_lower_c': -10,
    'inner_broiler.cmd_preheat_temp_c': 480.0,
    'inner_broiler.cmd_temp_c': 480.0,
    'outer_broiler.control_temp_delta_upper_c': 2,
    'outer_broiler.control_temp_delta_lower_c': -2,
    'outer_broiler.cmd_preheat_temp_c': 400.0,
    'outer_broiler.cmd_temp_c': 400.0,
}

param_2 = {
    'griddle.min_pressure_psi': .29
}

param_3 = {
    'griddle.valve.min_pressure_psi': 0.3,
    'inner_broiler.valve.min_pressure_psi': 0.3,
    'outer_broiler.valve.min_pressure_psi': 0.3,
}

params = param_3

kitchen_number = '4'

def get_state():
    # get the current state
    response = requests.get(
        'http://kitchen{}.corp.serveautomation.com/api/rpc/stations/pepperoni/parameters'.format(kitchen_number))
    return response.json()['state']


def update_params(i, params):

    start_string = 'http://kitchen{}.corp.serveautomation.com/api/rpc/stations/oven-'.format(kitchen_number)
    response = requests.patch(start_string + f'{i}/parameters',
                              json=params)
    print(response.json())


def run(params):
    for i in range(1, 5):
        print(i)
        update_params(i, params)


# run(params)
