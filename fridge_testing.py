from cProfile import label
from cgi import test
from datetime import datetime, timedelta
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def run():
    graphtec_file = r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-269 NSF fridge testing - dough storage\graphtec.xlsx'
    grafana_file = r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-269 NSF fridge testing - dough storage\grafana.xlsx'
    intertek_file = r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-269 NSF fridge testing - dough storage\intertek.xlsx'

    # changed the column names in the spreadsheets

    # graphtec has
    # XP_YP_ZP	XP_YM_ZP	XM_YM_ZP	XM_YP_ZP	XP_YP_ZM	XP_YM_ZM	XM_YM_ZM	XM_YP_ZM	Chamber_Control	Fridge_Current	Interior_XM_ZP	Interior_Center	Interior_XP_ZM
    # data rate = 1 Hz

    # grafana has
    # Interior_Center	Interior_XM_ZP	Interior_XP_ZM	air_temperature_c	defrost_temperature_c	controller.compressor
    # data rate = ~4 Hz
    # compressor is 0 or 1 not boolean
    # has many NaNs

    # intertek
    # time is not correct. Converted a column to datetime in excel.
    # channels
    # XP_YP_ZP	XP_YM_ZP	XM_YM_ZP	XM_YP_ZP	XP_YP_ZM	XP_YM_ZM	XM_YM_ZM	XM_YP_ZM	Interior_XM_ZP	Interior_Center	Interior_XP_ZM
    # data rate = 0.1 Hz


    # import all data to their own data-frames
    graphtec_df = pd.read_excel(graphtec_file)
    grafana_df = pd.read_excel(grafana_file)
    intertek_df = pd.read_excel(intertek_file)
    # get rid of the default time channels
    intertek_df.drop(['Date', 'Timer'], axis='columns', inplace=True)
    # print(graphtec_df.head(n=15))
    # print(grafana_df.head(n=15))
    # print(intertek_df.head(n=15))

    # set time to index
    grafana_df.set_index('Time', drop=True, inplace=True)
    graphtec_df.set_index('Time', drop=True, inplace=True)
    intertek_df.set_index('Time', drop=True, inplace=True)

    # grafana has duplicate index columns, both because time is read in seconds but
    # the sampling rate is 4 Hz, and also because the compressor data gets its own line.
    # group the data and downselect to 1Hz.
    grafana_df = grafana_df.groupby(level=0).mean()
    graphtec_df = graphtec_df.groupby(level=0).mean()
    # print(grafana_df.head(n=15))


    # downsample grafana data to go to 1 Hz. backfill NaNs.
    grafana_df = grafana_df.resample('1S').bfill()
    graphtec_df = graphtec_df.resample('1S').bfill()
    # print(grafana_df.head(n=15))
    # print(graphtec_df.head(n=15))

    # merge on time - inner merge (keep only indexes available in both frames)
    serve_df = pd.merge(graphtec_df, grafana_df, how='inner', left_index=True, right_index=True, suffixes=('_graphtec', '_grafana'))
    # print(serve_df.head(n=15))

    # plot both frames and their join just to make sure it all lines up
    # grafana_df.plot()
    # graphtec_df.plot()
    # serve_df.plot()
    # plt.show()

    # the intertek data doesn't have a real marker. Data streams were stopped at the
    # same time so set end time to the same and back-calculate time signature at 10Hz.
    # last time captured.
    end_time = serve_df.index.values[-1]
    times = pd.date_range(end=end_time, periods=intertek_df.shape[0], freq='10S')

    # add these times as the intertek time reference and re-index.
    intertek_df['Time'] = times
    intertek_df.set_index('Time', drop=True, inplace=True)
    # intertek_df.plot()

    # Inner merge all data - this will downsample to 0.1Hz
    df = pd.merge(serve_df, intertek_df, how='inner', left_index=True, right_index=True, suffixes=('_serve', '_intertek'))
    df.plot()
    plt.show()

    # Data streams are now fixed and aligned at 0.1Hz
    save = False
    if(save):
        base_path = r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Tests\Test-269 NSF fridge testing - dough storage'
        df.to_excel(base_path + r'\merged_data.xlsx')


    # Keep channels with intertek or compressor
    cols = df.columns
    for col in cols:
        if any(chk in col for chk in ['serve', 'graphtec', 'grafana', 'temperature']):
            df.drop(col, axis='columns', inplace=True)

    df.plot()
    plt.show()

    # looking at just these times, 18:42:15 is a safe time all channels are still in
    # the correct temperature ranges. Could do this with parsing, but not really necessary.
    # This means the window is 14:42:15 - 18:42:15
    test_data_df = df.truncate('2022-02-23 14:42:15', '2022-02-23 18:42:15')
    test_data_df.plot()
    plt.show()

    # there's some cleaner plotting to do here with subplots of similar data streams
    f, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex='all')

    # chamber temps
    chamber_cols = ['']
    for col in test_data_df.columns:
        if 'Interior' not in col and ('XM' in col or 'XP' in col):
            ax1.plot(test_data_df.index.values, test_data_df[col].values, label=col)
    ax1.set_ylim(35,45)
    ax1.axhline(y=36, color='r')
    ax1.set_ylabel('Temperature [c]')
    ax1.legend()

    # fridge temps
    for col in test_data_df.columns:
        if 'Interior' in col:
            ax2.plot(test_data_df.index.values, test_data_df[col].values, label=col)
    ax2.set_ylim(0,4)
    ax2.set_ylabel('Temperature [c]')
    ax2.legend()

    # compressor cycle
    ax3.plot(test_data_df.index.values, test_data_df['controller.compressor'], label='compressor')
    ax3.set_ylabel('compressor on/off')
    ax3.legend()
    ax3.set_xlabel('Time')

    f.suptitle('Dough-storage results')
    plt.show()

    # calculate duty cycle of the entire time
    duty_cycle = test_data_df['controller.compressor'].sum() / test_data_df.shape[0]
    print(duty_cycle)