from nturl2path import url2pathname
import requests
import time
import threading
import random

def get_state(kitchen, station):
    # get the current state
    url = f'http://{kitchen}.corp.serveautomation.com/api/rpc/stations/{station}/parameters'
    response = requests.get(url)
    # print(response.json())
    return response.json()['state']

    if spoof:
        responses = ['wait_for_entry', 'wait_for_exit']
        res = random.choice(responses)
        print(f'{station} {res}')
        return res


def entry(kitchen, station):
    # command entry routine
    url = f'http://{kitchen}.corp.serveautomation.com/api/rpc/stations/{station}/actions/entry_routine'
    # print(f'{station} entry')
    requests.post(url)



def exit(kitchen, station):
    # command exit routine
    url = f'http://{kitchen}.corp.serveautomation.com/api/rpc/stations/{station}/actions/exit_routine'
    requests.post(url)
    # print(f'{station} exit')



def run_stations(n, kitchen, stations):
    for i in range(n):

        # run entry on the first station, when it is in wait for exit and the 
        # next is in wait for entry command them both at the same time.
        # on the last station, just have it run exit. Then start again.

        for j in range(len(stations)):
            # run entry on j
            entry(kitchen, stations[j])
            # wait until j is in wait for exit and j+1 is in wait for entry
            try:
                while(
                    not (get_state(kitchen, stations[j]) == 'wait_for_exit' and 
                        get_state(kitchen, stations[j+1]) == 'wait_for_entry')
                ):
                    time.sleep(1)
            except IndexError:
                while(not (get_state(kitchen, stations[j]) == 'wait_for_exit')):
                    time.sleep(1)
            # run exit on station j-1
            exit(kitchen, stations[j])
            # the next station will get triggered when this loop loops.


def run():
    kitchen = 'kitchen3'
    stations = ['dough-flipper','dough-press']

    run_stations(1, kitchen, stations)
